* regenerated at 13.05.2018 08:00:54
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZTEST_DBTOP.                      " Global Declarations
  INCLUDE LZTEST_DBUXX.                      " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZTEST_DBF...                      " Subroutines
* INCLUDE LZTEST_DBO...                      " PBO-Modules
* INCLUDE LZTEST_DBI...                      " PAI-Modules
* INCLUDE LZTEST_DBE...                      " Events
* INCLUDE LZTEST_DBP...                      " Local class implement.
* INCLUDE LZTEST_DBT99.                      " ABAP Unit tests
  INCLUDE LZTEST_DBF00                            . " subprograms
  INCLUDE LZTEST_DBI00                            . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
