*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 13.05.2018 at 08:00:54
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZTEST_DB........................................*
DATA:  BEGIN OF STATUS_ZTEST_DB                      .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZTEST_DB                      .
CONTROLS: TCTRL_ZTEST_DB
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZTEST_DB                      .
TABLES: ZTEST_DB                       .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
